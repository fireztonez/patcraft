<!-- Merci d'avoir soumis un problème pour le sujet concerné. Veuillez vous assurer que vous remplissez toutes les informations requises requises par le modèle ci-dessous. -->
<!-- Remarque: Lorsque vous signalez un Bug, assurez-vous que les logs ont été téléchargés dans Hastebin/PasteBin/Gist/Snippet etc. -->
<!-- REMARQUE: Si vous avez d’autres mods installés ou si vous avez changé de version; veuillez revenir à une installation propre et tester à nouveau avec un Crash/Bug avant de poster. -->

## Bug Report
<!--- Si vous décrivez un bug, décrivez le comportement actuel -->
<!--- Si vous suggérez un changement / une amélioration, dites-nous comment cela devrait fonctionner -->

## Expected Behaviour
<!--- Si vous décrivez un Bug, dites-nous ce qui se passe au lieu du comportement attendu -->
<!--- Si vous suggérez un changement/une amélioration, expliquez la différence par rapport au comportement actuel -->

## Possible Solution
<!--- Non obligatoire, mais suggère un correctif/une raison pour le Bug -->
<!--- ou des idées sur la façon de mettre en œuvre l'ajout ou le changement -->

## Steps to Reproduce (for bugs)
<!--- Fournissez un lien vers un exemple en direct ou un ensemble d'étapes sans ambiguïté pour reproduire le problème: -->
1.
2.
<!--- Add more if needed -->

## Logs
<!-- Les journaux Twitch se trouvent dans le répertoire d'installation de l'application Twitch. Ou cliquez sur le bouton... sur PatCraft et cliquez sur "Ouvrir le dossier" -->
<!-- Puis téléchargez le dernier "Crash Reports"/Logs to Hastebin/PasteBin/Gist/Snippet. Ne pas envoyé en fichier sur le GitLab -->
* Client/Server Log:
* Crash Log:

## Client Information
<!--- Incluez autant de détails pertinents sur l'environnement dans lequel vous avez rencontré le Bug -->
<!-- Merci de nous dire combien de mémoire vous avez alloué au jeu. Pour Twitch, regardez dans les paramètres. -->
* Modpack Version:
* Java Version:
* Launcher Used:
* Memory Allocated:
* Server/LAN/Single Player:
* Optifine Installed:
* Shaders Enabled:

<!--- Informations supplémentaires si vous utilisez une configuration de serveur (SUPPRIMEZ CETTE SECTION SI VOTRE ENJEU N'EST QUE LE CLIENT) -->
## Server Information
* PatBoil/Fireztonez SubServer (Oui/Non):
<!--- Sinon, veillez nous soumettre les informations sur le serveur -->
* Java Version:
* Operating System:
* Hoster/Hosting Solution:
* Sponge (Non-Vanilla Forge) Server:
